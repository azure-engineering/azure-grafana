resource "random_password" "admpasswd" {
 length = 12
 special = false
}

resource "azurerm_key_vault_secret" "grafana_adm" {
name         = "grafana-adm-password"
value        = random_password.admpasswd.result
key_vault_id = ""
}