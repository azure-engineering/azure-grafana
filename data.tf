data "azuread_client_config" "current" {}

data "azurerm_key_vault_secret" "dbpasswd" {
name         = "atn-devops-cloudquery-db"
key_vault_id = ""
}

data "azurerm_key_vault" "cloudquery" {
  name                = var.vault_name
  resource_group_name = var.vault_rg
  depends_on          = [azurerm_resource_group.iac]
}

data "azurerm_key_vault_secret" "app_secret" {
  name         = var.app_sec_name
  key_vault_id = data.azurerm_key_vault.cloudquery.id
}

data "azurerm_subnet" "snet_devops_postgres" {
  name                 = ""
  virtual_network_name = ""
  resource_group_name  = ""
}

data "azurerm_virtual_network" "vnet_devops" {
  name                = ""
  resource_group_name = ""
}

data "azurerm_subnet" "snet_devops" {
  name                 = ""
  virtual_network_name = ""
  resource_group_name  = ""
}