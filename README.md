## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azuread"></a> [azuread](#provider\_azuread) | n/a |
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | n/a |
| <a name="provider_azurerm.identity"></a> [azurerm.identity](#provider\_azurerm.identity) | n/a |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_key_vault.iac](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault) | resource |
| [azurerm_key_vault_key.iac](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_key) | resource |
| [azurerm_key_vault_secret.db_password](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret) | resource |
| [azurerm_key_vault_secret.grafana_adm](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret) | resource |
| [azurerm_key_vault_secret.grafana_password](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret) | resource |
| [azurerm_network_interface.devops_iac](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface) | resource |
| [azurerm_postgresql_flexible_server.cloudquery](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/postgresql_flexible_server) | resource |
| [azurerm_postgresql_flexible_server_configuration.azure_flexible_postgres_extensions](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/postgresql_flexible_server_configuration) | resource |
| [azurerm_postgresql_flexible_server_database.cloudquery_db](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/postgresql_flexible_server_database) | resource |
| [azurerm_private_dns_a_record.grafana](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_dns_a_record) | resource |
| [azurerm_private_dns_zone.cloudquery_postgres_private_dns_zone](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_dns_zone) | resource |
| [azurerm_private_dns_zone_virtual_network_link.db_dns_link](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/private_dns_zone_virtual_network_link) | resource |
| [azurerm_resource_group.iac](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) | resource |
| [helm_release.grafana](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.grafana](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [random_password.admpasswd](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.db_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_password.grafana_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [random_string.random_string](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [azuread_client_config.current](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/data-sources/client_config) | data source |
| [azurerm_key_vault.cloudquery](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/key_vault) | data source |
| [azurerm_key_vault_secret.app_secret](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/key_vault_secret) | data source |
| [azurerm_key_vault_secret.dbpasswd](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/key_vault_secret) | data source |
| [azurerm_kubernetes_cluster.k8s_cluster](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/kubernetes_cluster) | data source |
| [azurerm_subnet.snet_devops](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/subnet) | data source |
| [azurerm_subnet.snet_devops_postgres](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/subnet) | data source |
| [azurerm_virtual_network.vnet_devops](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/virtual_network) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_sec_name"></a> [app\_sec\_name](#input\_app\_sec\_name) | Name of the Key Vault secret for the application secret | `string` | n/a | yes |
| <a name="input_azure_location"></a> [azure\_location](#input\_azure\_location) | azure location | `string` | `""` | no |
| <a name="input_client_id"></a> [client\_id](#input\_client\_id) | Azure AD Client ID | `string` | n/a | yes |
| <a name="input_db_login"></a> [db\_login](#input\_db\_login) | Administrator login for Database | `string` | `"cloudquery"` | no |
| <a name="input_db_name"></a> [db\_name](#input\_db\_name) | Name of Database | `string` | `"cloudquery"` | no |
| <a name="input_default_name"></a> [default\_name](#input\_default\_name) | Suffix or prefix | `string` | `""` | no |
| <a name="input_default_tags"></a> [default\_tags](#input\_default\_tags) | Default tags to be applied to resources | `map(string)` | `{}` | no |
| <a name="input_devops_rg_default_name"></a> [devops\_rg\_default\_name](#input\_devops\_rg\_default\_name) | Name of RG that already exists in subscription | `string` | `""` | no |
| <a name="input_dns_a_record_name_grafana_ttl"></a> [dns\_a\_record\_name\_grafana\_ttl](#input\_dns\_a\_record\_name\_grafana\_ttl) | TTL (Time to Live) for the Grafana A record in seconds | `number` | n/a | yes |
| <a name="input_dns_zone_name_a_record"></a> [dns\_zone\_name\_a\_record](#input\_dns\_zone\_name\_a\_record) | Name of the private DNS zone for the A record | `string` | n/a | yes |
| <a name="input_dns_zone_name_rg"></a> [dns\_zone\_name\_rg](#input\_dns\_zone\_name\_rg) | Name of the resource group containing the private DNS zone | `string` | n/a | yes |
| <a name="input_encryption_type"></a> [encryption\_type](#input\_encryption\_type) | encryption type for disk encryption | `string` | `"EncryptionAtRestWithPlatformAndCustomerKeys"` | no |
| <a name="input_grafana_dns"></a> [grafana\_dns](#input\_grafana\_dns) | Name of the A record for Grafana in the private DNS zone | `string` | n/a | yes |
| <a name="input_grafana_sec_name"></a> [grafana\_sec\_name](#input\_grafana\_sec\_name) | Name of the Key Vault secret for the Grafana password | `string` | n/a | yes |
| <a name="input_keyvault_ret_days"></a> [keyvault\_ret\_days](#input\_keyvault\_ret\_days) | soft delete retention days | `number` | `7` | no |
| <a name="input_pg_fs_version"></a> [pg\_fs\_version](#input\_pg\_fs\_version) | Version for Postgres Flexible Server | `number` | `14` | no |
| <a name="input_postgres_backup_retention_days"></a> [postgres\_backup\_retention\_days](#input\_postgres\_backup\_retention\_days) | backup retention in days for postgres backups | `number` | `30` | no |
| <a name="input_postgres_sku_name"></a> [postgres\_sku\_name](#input\_postgres\_sku\_name) | sku name of the instance size of the postgres database | `string` | `"GP_Standard_D2ds_v4"` | no |
| <a name="input_postgres_storage_mb"></a> [postgres\_storage\_mb](#input\_postgres\_storage\_mb) | mb limit of the postgres database instance | `number` | `32768` | no |
| <a name="input_subnet_name"></a> [subnet\_name](#input\_subnet\_name) | Name of the subnet | `string` | n/a | yes |
| <a name="input_tenant_id"></a> [tenant\_id](#input\_tenant\_id) | Azure AD Tenant ID | `string` | n/a | yes |
| <a name="input_vault_name"></a> [vault\_name](#input\_vault\_name) | Name of the Azure Key Vault | `string` | n/a | yes |
| <a name="input_vault_rg"></a> [vault\_rg](#input\_vault\_rg) | Name of the Azure Resource Group containing the Key Vault | `string` | n/a | yes |

## Outputs

No outputs.
