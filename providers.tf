data "azurerm_kubernetes_cluster" "k8s_cluster" {
  name                = var.cluster-name
  resource_group_name = var.cluster-rg
}


provider "kubernetes" {
  host                   = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.host
  username               = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.username
  password               = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.password
  client_certificate     = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.client_certificate)
  client_key             = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.client_key)
  cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.host
    username               = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.username
    password               = data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.password
    client_certificate     = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.client_certificate)
    client_key             = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.client_key)
    cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.k8s_cluster.kube_admin_config.0.cluster_ca_certificate)
  }
}


terraform {
  backend "azurerm" {
    resource_group_name  = ""
    storage_account_name = ""
    container_name       = ""
    key                  = ""
    environment          = ""
    subscription_id      = ""
  }
}


provider "azurerm" {
  features {
    key_vault {
      purge_soft_deleted_certificates_on_destroy = true
    }
  }
  # Configuration options
  environment     = ""
  subscription_id = ""
  tenant_id       = ""
}