resource "azurerm_private_dns_a_record" "grafana" {
  name                = var.grafana_dns
  zone_name           = var.dns_zone_name_a_record
  resource_group_name = var.dns_zone_name_rg
  ttl                 = var.dns_a_record_name_grafana_ttl
  records             = ["IP Address of LB"]
  provider            = azurerm.identity

  depends_on = [
    helm_release.grafana
  ]

  tags = var.default_tags

}