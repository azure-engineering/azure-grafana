variable "vault_name" {
  description = "Name of the Azure Key Vault"
  type        = string
}

variable "vault_rg" {
  description = "Name of the Azure Resource Group containing the Key Vault"
  type        = string
}

variable "grafana_sec_name" {
  description = "Name of the Key Vault secret for the Grafana password"
  type        = string
}

variable "app_sec_name" {
  description = "Name of the Key Vault secret for the application secret"
  type        = string
}

variable "client_id" {
  description = "Azure AD Client ID"
  type        = string
}

variable "tenant_id" {
  description = "Azure AD Tenant ID"
  type        = string
}

variable "subnet_name" {
  description = "Name of the subnet"
  type        = string
}

variable "grafana_dns" {
  description = "Name of the A record for Grafana in the private DNS zone"
  type        = string
}

variable "dns_zone_name_a_record" {
  description = "Name of the private DNS zone for the A record"
  type        = string
}

variable "dns_zone_name_rg" {
  description = "Name of the resource group containing the private DNS zone"
  type        = string
}

variable "dns_a_record_name_grafana_ttl" {
  description = "TTL (Time to Live) for the Grafana A record in seconds"
  type        = number
}

variable "default_tags" {
  description = "Default tags to be applied to resources"
  type        = map(string)
  default     = {}
}

variable "postgres_sku_name" {
  description = "sku name of the instance size of the postgres database"
  type        = string
  default     = "GP_Standard_D2ds_v4"
}

variable "postgres_backup_retention_days" {
  description = "backup retention in days for postgres backups"
  type        = number
  default     = 30
}

variable "postgres_storage_mb" {
  description = "mb limit of the postgres database instance"
  type        = number
  default     = 32768
}

variable "db_name" {
  description = "Name of Database"
  type        = string
  default     = "cloudquery"
}

variable "db_login" {
  description = "Administrator login for Database"
  type        = string
  default     = "cloudquery"
}

variable "pg_fs_version" {
  description = "Version for Postgres Flexible Server"
  type        = number
  default     = 14
}

variable "keyvault_ret_days" {
  description = "soft delete retention days"
  type        = number
  default     = 7
}

variable "encryption_type" {
  description = "encryption type for disk encryption"
  type        = string
  default     = "EncryptionAtRestWithPlatformAndCustomerKeys"
}

variable "azure_location" {
  description = "azure location"
  type        = string
  default     = ""
}

variable "default_name" {
  description = "Suffix or prefix"
  type        = string
  default     = ""
}

variable "devops_rg_default_name" {
  description = "Name of RG that already exists in subscription"
  type        = string
  default     = ""
}