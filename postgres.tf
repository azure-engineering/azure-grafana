resource "azurerm_resource_group" "iac" {
  name     = var.default_name
  location = var.location
  tags     = var.default_tags
}


resource "azurerm_postgresql_flexible_server" "cloudquery" {
  name                   = "${var.default_name}-db"
  resource_group_name    = var.default_name
  location               = var.azure_location
  version                = var.pg_fs_version
  backup_retention_days  = var.postgres_backup_retention_days
  administrator_login    = var.db_login
  administrator_password = random_password.db_password.result
  sku_name               = var.postgres_sku_name
  zone                   = var.zone
  storage_mb             = var.postgres_storage_mb
  private_dns_zone_id    = azurerm_private_dns_zone.cloudquery_postgres_private_dns_zone.id
  delegated_subnet_id    = data.azurerm_subnet.snet_devops_postgres.id
  high_availability {
    mode = "ZoneRedundant"
  }

  lifecycle {
    ignore_changes = [
      zone, high_availability[0].standby_availability_zone,
    ]
  }

  depends_on = [azurerm_private_dns_zone_virtual_network_link.db_dns_link, ]
  tags       = var.default_tags
}

resource "azurerm_postgresql_flexible_server_configuration" "azure_flexible_postgres_extensions" {
  name      = "azure.extensions"
  server_id = azurerm_postgresql_flexible_server.cloudquery.id
  value     = "PG_TRGM,BTREE_GIST"
}

resource "azurerm_postgresql_flexible_server_database" "cloudquery_db" {
  name      = var.db_name
  server_id = azurerm_postgresql_flexible_server.cloudquery.id
}

resource "azurerm_private_dns_zone" "cloudquery_postgres_private_dns_zone" {
  name                = "cloudquery${random_string.random_string.result}.private.postgres.database.usgovcloudapi.net"
  resource_group_name = var.default_name
  tags                = var.default_tags

  depends_on = [azurerm_resource_group.iac]
}

resource "azurerm_private_dns_zone_virtual_network_link" "db_dns_link" {
  name                  = "${var.default_name}-cloudquery-postgres-db-dns-link"
  resource_group_name   = var.default_name
  private_dns_zone_name = azurerm_private_dns_zone.cloudquery_postgres_private_dns_zone.name
  virtual_network_id    = data.azurerm_virtual_network.vnet_devops.id
  tags                  = var.default_tags
}

resource "random_password" "db_password" {
  length  = 12
  special = false
  upper   = true
}

resource "azurerm_network_interface" "devops_iac" {
  name                = "${var.default_name}-nic"
  location            = azurerm_resource_group.iac.location
  resource_group_name = azurerm_resource_group.iac.name

  ip_configuration {
    name                          = "configuration"
    subnet_id                     = data.azurerm_subnet.snet_devops.id
    private_ip_address_allocation = "Dynamic"
  }
  tags = var.default_tags
}

resource "azurerm_key_vault" "iac" {
  depends_on                      = [azurerm_resource_group.iac]
  name                            = var.default_name
  location                        = azurerm_resource_group.iac.location
  resource_group_name             = azurerm_resource_group.iac.name
  enabled_for_disk_encryption     = true
  tenant_id                       = data.azuread_client_config.current.tenant_id
  soft_delete_retention_days      = var.keyvault_ret_days
  purge_protection_enabled        = true
  sku_name                        = "standard"
  tags                            = var.default_tags
  enabled_for_deployment          = true
  enabled_for_template_deployment = true

  network_acls {
    bypass         = "AzureServices"
    default_action = "Allow"
  }

  access_policy {
    tenant_id = data.azuread_client_config.current.tenant_id
    object_id = data.azuread_client_config.current.object_id

    key_permissions = [
      "Get",
      "List",
      "Update",
      "Create",
      "Import",
      "Delete",
      "Recover",
      "Backup",
      "Restore",
      "GetRotationPolicy",
      "SetRotationPolicy",
      "Rotate",
      "WrapKey",
      "UnwrapKey"
    ]
    secret_permissions = [
      "Get",
      "List",
      "Set",
      "Delete",
      "Recover",
      "Backup",
      "Restore"
    ]
    certificate_permissions = [
      "Get",
      "List",
      "Update",
      "Create",
      "Delete",
      "Purge",
      "Recover"
    ]
  }
}

resource "azurerm_key_vault_key" "iac" {
  depends_on   = [azurerm_key_vault.iac]
  name         = "${var.default_name}-${random_string.random_string.result}"
  key_vault_id = azurerm_key_vault.iac.id
  key_type     = "RSA"
  key_size     = 2048

  lifecycle {
    ignore_changes = [
      key_vault_id,
    ]
  }
  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "random_string" "random_string" {
  length  = 7
  special = false
  lower   = true
  upper   = false
}

resource "azurerm_key_vault_secret" "db_password" {
  name = "${var.default_name}-db"
  value = random_password.db_password.result
  key_vault_id = data.azurerm_key_vault.cloudquery.id
}