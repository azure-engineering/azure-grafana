adminUser: admin
adminPassword: $${grafana_password}
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: "nginx"
  path: /
  hosts:
    - ${dns_name_of_grafana_instance}
  tls:
    - secretName: grafana-tls
      hosts:
        - ${dns_name_of_grafana_instance}
service:
  enabled: true
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
    service.beta.kubernetes.io/azure-load-balancer-internal-subnet: "${subnet_name}"
grafana.ini:
  server:
    root_url: ${url_of_grafana_instance}
  auth.azuread:
    name: Azure AD
    enabled: true
    allow_sign_up: true
    auto_login: false
    client_id: $${client_id}
    client_secret: $${client_secret}
    scopes: openid email profile
    auth_url: https://login.microsoftonline.us/$${tenant_id}/oauth2