resource "kubernetes_namespace" "grafana" {
  metadata {
    name = "grafana"
  }
}

resource "random_password" "grafana_password" {
  length             = 16
  special            = true
  override_special   = "!#$%^&"
  lower              = true
  upper              = true
  numeric            = true
}

resource "azurerm_key_vault_secret" "grafana_password" {
  name         = var.grafana_sec_name
  value        = random_password.grafana_password.result
  key_vault_id = data.azurerm_key_vault.cloudquery.id
}

resource "helm_release" "grafana" {
  name       = "grafana"
  repository = "https://grafana.github.io/helm-charts/"
  chart      = "grafana"
  version    = "6.52.3"
  namespace  = "grafana"

  values = [templatefile("${path.module}/grafana-values.tpl", {
    grafana_password        = azurerm_key_vault_secret.grafana_password.value
    client_id               = var.client_id
    client_secret           = data.azurerm_key_vault_secret.app_secret.value
    tenant_id               = var.tenant_id
    dns_name_of_grafana_instance = var.grafana_dns
    subnet_name             = var.subnet_name
    url_of_grafana_instance = var.grafana_dns
  })]

  depends_on = [
    kubernetes_namespace.grafana
  ]
}